/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itesm.Ejbs;

import com.itesm.Models.Elemento;
import com.itesm.Models.Patrulla;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author jacobotapia
 */
@Stateless
public class PatrullaFacade extends AbstractFacade<Patrulla> implements PatrullaFacadeLocal {

    @PersistenceContext(unitName = "WebCopPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public PatrullaFacade() {
        super(Patrulla.class);
    }

    @Override
    public Patrulla findByPlacas(String placas) {
        Query q = em.createQuery("SELECT p FROM Patrulla p WHERE p.placas = :p AND p.activo = :a", Patrulla.class);
        q.setParameter("a", true);
        q.setParameter("p", placas);
        return (Patrulla) q.getSingleResult();
    }

    @Override
    public List<Patrulla> findAllActivo() {
        Query q = em.createQuery("SELECT p FROM Patrulla p WHERE p.activo = :a ORDER BY p.idPatrulla DESC", Elemento.class);
        q.setParameter("a", true);
        return q.getResultList();
    }

    @Override
    public Patrulla findByPlacas2(String placas) {
       try{
           Query q = em.createQuery("SELECT p FROM Patrulla p WHERE p.placas = :p AND p.activo = :a", Patrulla.class);
           q.setParameter("a", true);
           q.setParameter("p", placas);
           return (Patrulla) q.getSingleResult();
       }catch(NoResultException e){
           return null;
       }
    }

    @Override
    public void delete(Integer id) {
        Query q = em.createQuery("Update Patrulla p SET p.activo = :a WHERE p.idPatrulla = :id");
        q.setParameter("a", false);
        q.setParameter("id", id);
        q.executeUpdate();
    }
    
}
