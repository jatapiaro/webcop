/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itesm.Ejbs;

import com.itesm.Models.Camara;
import com.itesm.Models.Sector;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author jacobotapia
 */
@Stateless
public class CamaraFacade extends AbstractFacade<Camara> implements CamaraFacadeLocal {

    @PersistenceContext(unitName = "WebCopPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CamaraFacade() {
        super(Camara.class);
    }
    
    @Override
    public List<Camara> findAll(){
        Query q = em.createQuery("SELECT c FROM Camara c WHERE c.activo = :a ORDER BY c.idCamara DESC", Camara.class);
        q.setParameter("a", true);
        return q.getResultList();
    }

    @Override
    public void delete(Integer id) {
        Query q = em.createQuery("Update Camara c SET c.activo = :a WHERE c.idCamara = :id");
        q.setParameter("a", false);
        q.setParameter("id", id);
        q.executeUpdate();
    }

    @Override
    public Camara findByIdentificador(int identificador) {
        try {
            Query q = em.createQuery("SELECT c FROM Camara c WHERE c.identificador = :i AND c.activo = :a", Camara.class);
            q.setParameter("a", true);
            q.setParameter("i", identificador);
            return (Camara) q.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }
    
}
