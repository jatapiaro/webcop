/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itesm.Ejbs;

import com.itesm.Models.ElementoReporte;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author jacobotapia
 */
@Stateless
public class ElementoReporteFacade extends AbstractFacade<ElementoReporte> implements ElementoReporteFacadeLocal {

    @PersistenceContext(unitName = "WebCopPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ElementoReporteFacade() {
        super(ElementoReporte.class);
    }
    
}
