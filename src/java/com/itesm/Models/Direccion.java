/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itesm.Models;

import com.itesm.Dao.Dao;
import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author jacobotapia
 */
@Entity
@Table(name = "Direccion")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Direccion.findAll", query = "SELECT d FROM Direccion d")
    , @NamedQuery(name = "Direccion.findByCodigoPostal", query = "SELECT d FROM Direccion d WHERE d.codigoPostal = :codigoPostal")})
public class Direccion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "codigoPostal")
    private Integer codigoPostal;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "colonia")
    private String colonia;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "municipio")
    private String municipio;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "estado")
    private String estado;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "direccionID")
    private List<InfractorReporte> infractorReporteList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "direccionID")
    private List<Reporte> reporteList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "direccionID")
    private List<Testigo> testigoList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "direccionID")
    private List<Denunciante> denuncianteList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idDireccion")
    private List<Camara> camaraList;
    @Transient
    private boolean decrypted;

    public Direccion() {
        this.decrypted = false;
    }

    public Direccion(Integer codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    public Direccion(Integer codigoPostal, String colonia, String municipio, String estado) {
        this.codigoPostal = codigoPostal;
        this.colonia = colonia;
        this.municipio = municipio;
        this.estado = estado;
    }

    public Integer getCodigoPostal() {
        return codigoPostal;
    }

    public void setCodigoPostal(Integer codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    public String getColonia() {
        return colonia;
    }

    public void setColonia(String colonia) {
        this.colonia = colonia;
    }

    public String getMunicipio() {
        return municipio;
    }

    public void setMunicipio(String municipio) {
        this.municipio = municipio;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @XmlTransient
    public List<InfractorReporte> getInfractorReporteList() {
        return infractorReporteList;
    }

    public void setInfractorReporteList(List<InfractorReporte> infractorReporteList) {
        this.infractorReporteList = infractorReporteList;
    }

    @XmlTransient
    public List<Reporte> getReporteList() {
        return reporteList;
    }

    public void setReporteList(List<Reporte> reporteList) {
        this.reporteList = reporteList;
    }

    @XmlTransient
    public List<Testigo> getTestigoList() {
        return testigoList;
    }

    public void setTestigoList(List<Testigo> testigoList) {
        this.testigoList = testigoList;
    }

    @XmlTransient
    public List<Denunciante> getDenuncianteList() {
        return denuncianteList;
    }

    public void setDenuncianteList(List<Denunciante> denuncianteList) {
        this.denuncianteList = denuncianteList;
    }

    @XmlTransient
    public List<Camara> getCamaraList() {
        return camaraList;
    }

    public void setCamaraList(List<Camara> camaraList) {
        this.camaraList = camaraList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codigoPostal != null ? codigoPostal.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Direccion)) {
            return false;
        }
        Direccion other = (Direccion) object;
        if ((this.codigoPostal == null && other.codigoPostal != null) || (this.codigoPostal != null && !this.codigoPostal.equals(other.codigoPostal))) {
            return false;
        }
        return true;
    }
    
    @Override
    public String toString() {
        String s = "Estado: " + this.estado + ", Municipio: " + this.municipio + ""
                + ", Colonia: " + this.colonia + ", Código Postal: " + this.codigoPostal;
        if (s.equals("Estado: null, Municipio: null, Colonia: null, Código Postal: null")) {
            return "Dirección no encontrada";
        } else {
            return s;
        }
    }

    public void decrypt() throws Exception {
        if(!this.decrypted){
            Dao d = new Dao();
            this.estado = d.decrypt(this.estado);
            this.municipio = d.decrypt(this.municipio);
            this.colonia = d.decrypt(this.colonia);
            this.decrypted = true;
        }
    }
    
}
