/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itesm.Controllers;

import com.itesm.Ejbs.ElementoFacadeLocal;
import com.itesm.Models.Elemento;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.primefaces.context.RequestContext;

/**
 *
 * @author jacobotapia
 */
@Named
@ViewScoped
public class ElementoController implements Serializable{
    
    @EJB
    private ElementoFacadeLocal elementoEJB;
    private Elemento elemento;
    private List<Elemento> elementos;
    
    @PostConstruct
    public void init(){
        elemento=new Elemento();
        elementos=new ArrayList<Elemento>();
    }
    
    public void listar() throws Exception{
        this.elementos=this.elementoEJB.findAllActivos();
        for(Elemento e : elementos){
            e.decrypt();
        }
    }
    
    public void crear() throws Exception{
        if(this.elementoEJB.findByPlaca2(this.elemento.getPlaca())==null){
            this.elemento.setActivo(true);
            this.elemento.encrypt();
            this.elementoEJB.create(elemento);
            this.elemento = null;
            this.elemento = new Elemento();
            RequestContext.getCurrentInstance().execute("PF('widgetDatos').hide();");
        }else{
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Este elemento/placa ya está registrado", "Este elemento/placa ya está registrado"));
        }
    }
    
    public void editar() throws Exception{
        this.elemento.encrypt2();
        this.elementoEJB.edit(elemento);
        this.elemento = null;
        this.elemento = new Elemento();
    }
    
    public void findForEdit(Object id) throws Exception{
        this.elemento = this.elementoEJB.find(id);
        this.elemento.decrypt();
    }
    
    public void eliminar(Integer id){
        this.elementoEJB.delete(id);
    }
    

    /**
     * @return the elemento
     */
    public Elemento getElemento() {
        return elemento;
    }

    /**
     * @param elemento the elemento to set
     */
    public void setElemento(Elemento elemento) {
        this.elemento = elemento;
    }

    /**
     * @return the elementos
     */
    public List<Elemento> getElementos() {
        return elementos;
    }

    /**
     * @param elementos the elementos to set
     */
    public void setElementos(List<Elemento> elementos) {
        this.elementos = elementos;
    }
    
}
