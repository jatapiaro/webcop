/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itesm.Models;

import com.itesm.Dao.Dao;
import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.servlet.http.Part;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author jacobotapia
 */
@Entity
@Table(name = "InfractorReporte")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "InfractorReporte.findAll", query = "SELECT i FROM InfractorReporte i")
    , @NamedQuery(name = "InfractorReporte.findByIdInfractorReporte", query = "SELECT i FROM InfractorReporte i WHERE i.idInfractorReporte = :idInfractorReporte")
    , @NamedQuery(name = "InfractorReporte.findByHasImage", query = "SELECT i FROM InfractorReporte i WHERE i.hasImage = :hasImage")})
public class InfractorReporte implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idInfractorReporte")
    private Integer idInfractorReporte;
    @Lob
    @Size(max = 65535)
    @Column(name = "imagen")
    private String imagen;
    @Basic(optional = false)
    @NotNull
    @Column(name = "hasImage")
    private boolean hasImage;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "delitos")
    private String delitos;
    @JoinColumn(name = "infractorID", referencedColumnName = "idInfractor")
    @ManyToOne(optional = false)
    private Infractor infractorID;
    @JoinColumn(name = "reporteID", referencedColumnName = "idReporte")
    @ManyToOne(optional = false)
    private Reporte reporteID;
    @JoinColumn(name = "direccionID", referencedColumnName = "codigoPostal")
    @ManyToOne(optional = true)
    private Direccion direccionID;
    @Transient
    private List<String> delitosSeleccionados;
    @Transient 
    private boolean decrypted;
    @Transient 
    private Part file;

    public InfractorReporte() {
        this.decrypted = false;
        this.infractorID = new Infractor();
        this.direccionID = new Direccion();
        this.imagen = null;
    }

    public InfractorReporte(Integer idInfractorReporte) {
        this.idInfractorReporte = idInfractorReporte;
    }

    public InfractorReporte(Integer idInfractorReporte, boolean hasImage, String delitos) {
        this.idInfractorReporte = idInfractorReporte;
        this.hasImage = hasImage;
        this.delitos = delitos;
    }

    public Integer getIdInfractorReporte() {
        return idInfractorReporte;
    }

    public void setIdInfractorReporte(Integer idInfractorReporte) {
        this.idInfractorReporte = idInfractorReporte;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public boolean getHasImage() {
        return hasImage;
    }

    public void setHasImage(boolean hasImage) {
        this.hasImage = hasImage;
    }

    public String getDelitos() {
        return delitos;
    }

    public void setDelitos(String delitos) {
        this.delitos = delitos;
    }

    public Infractor getInfractorID() {
        return infractorID;
    }

    public void setInfractorID(Infractor infractorID) {
        this.infractorID = infractorID;
    }

    public Reporte getReporteID() {
        return reporteID;
    }

    public void setReporteID(Reporte reporteID) {
        this.reporteID = reporteID;
    }

    public Direccion getDireccionID() {
        return direccionID;
    }

    public void setDireccionID(Direccion direccionID) {
        this.direccionID = direccionID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idInfractorReporte != null ? idInfractorReporte.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof InfractorReporte)) {
            return false;
        }
        InfractorReporte other = (InfractorReporte) object;
        if ((this.idInfractorReporte == null && other.idInfractorReporte != null) || (this.idInfractorReporte != null && !this.idInfractorReporte.equals(other.idInfractorReporte))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.itesm.Models.InfractorReporte[ idInfractorReporte=" + idInfractorReporte + " ]";
    }

    /**
     * @return the delitosSeleccionados
     */
    public List<String> getDelitosSeleccionados() {
        return delitosSeleccionados;
    }

    /**
     * @param delitosSeleccionados the delitosSeleccionados to set
     */
    public void setDelitosSeleccionados(List<String> delitosSeleccionados) {
        this.delitosSeleccionados = delitosSeleccionados;
    }
    
    public void makeDelitosString(){
        String s = "";
        for(String st : this.delitosSeleccionados){
            s+=st+", ";
        }
        this.delitos = s;
    }
    
    public void encrypt() throws Exception{
        Dao d = new Dao();
        this.delitos = d.encrypt(this.delitos);
        this.infractorID.encrypt();
    }
    
    public void decrypt() throws Exception {
        if(!decrypted){
            Dao d = new Dao();
            this.delitos = d.decrypt(this.delitos);
            this.infractorID.decrypt();
            this.decrypted = true;
        }
    }

    /**
     * @return the file
     */
    public Part getFile() {
        return file;
    }

    /**
     * @param file the file to set
     */
    public void setFile(Part file) {
        this.file = file;
    }
    
}
