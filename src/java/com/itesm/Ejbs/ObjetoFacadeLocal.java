/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itesm.Ejbs;

import com.itesm.Models.Objeto;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author jacobotapia
 */
@Local
public interface ObjetoFacadeLocal {

    void create(Objeto objeto);

    void edit(Objeto objeto);

    void remove(Objeto objeto);

    Objeto find(Object id);

    List<Objeto> findAll();

    List<Objeto> findRange(int[] range);

    int count();
    
}
