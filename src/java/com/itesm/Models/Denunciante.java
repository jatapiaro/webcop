/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itesm.Models;

import com.itesm.Dao.Dao;
import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author jacobotapia
 */
@Entity
@Table(name = "Denunciante")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Denunciante.findAll", query = "SELECT d FROM Denunciante d")
    , @NamedQuery(name = "Denunciante.findByIdDenunciante", query = "SELECT d FROM Denunciante d WHERE d.idDenunciante = :idDenunciante")})
public class Denunciante implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idDenunciante")
    private Integer idDenunciante;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "rfc")
    private String rfc;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "nombre")
    private String nombre;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "apellidoPaterno")
    private String apellidoPaterno;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "apellidoMaterno")
    private String apellidoMaterno;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "firma")
    private String firma;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "numeroExterior")
    private String numeroExterior;
    @Lob
    @Size(max = 65535)
    @Column(name = "numeroInterior")
    private String numeroInterior;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "calle")
    private String calle;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "denuncianteID")
    private List<Reporte> reporteList;
    @JoinColumn(name = "direccionID", referencedColumnName = "codigoPostal")
    @ManyToOne(optional = false)
    private Direccion direccionID;
    @Transient
    private boolean decrypted;

    public Denunciante() {
        this.decrypted = false;
        this.direccionID = new Direccion();
    }

    public Denunciante(Integer idDenunciante) {
        this.idDenunciante = idDenunciante;
    }

    public Denunciante(Integer idDenunciante, String rfc, String nombre, String apellidoPaterno, String apellidoMaterno, String firma, String numeroExterior, String calle) {
        this.idDenunciante = idDenunciante;
        this.rfc = rfc;
        this.nombre = nombre;
        this.apellidoPaterno = apellidoPaterno;
        this.apellidoMaterno = apellidoMaterno;
        this.firma = firma;
        this.numeroExterior = numeroExterior;
        this.calle = calle;
    }

    public Integer getIdDenunciante() {
        return idDenunciante;
    }

    public void setIdDenunciante(Integer idDenunciante) {
        this.idDenunciante = idDenunciante;
    }

    public String getRfc() {
        return rfc;
    }

    public void setRfc(String rfc) {
        this.rfc = rfc;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidoPaterno() {
        return apellidoPaterno;
    }

    public void setApellidoPaterno(String apellidoPaterno) {
        this.apellidoPaterno = apellidoPaterno;
    }

    public String getApellidoMaterno() {
        return apellidoMaterno;
    }

    public void setApellidoMaterno(String apellidoMaterno) {
        this.apellidoMaterno = apellidoMaterno;
    }

    public String getFirma() {
        return firma;
    }

    public void setFirma(String firma) {
        this.firma = firma;
    }

    public String getNumeroExterior() {
        return numeroExterior;
    }

    public void setNumeroExterior(String numeroExterior) {
        this.numeroExterior = numeroExterior;
    }

    public String getNumeroInterior() {
        return numeroInterior;
    }

    public void setNumeroInterior(String numeroInterior) {
        this.numeroInterior = numeroInterior;
    }

    public String getCalle() {
        return calle;
    }

    public void setCalle(String calle) {
        this.calle = calle;
    }

    @XmlTransient
    public List<Reporte> getReporteList() {
        return reporteList;
    }

    public void setReporteList(List<Reporte> reporteList) {
        this.reporteList = reporteList;
    }

    public Direccion getDireccionID() {
        return direccionID;
    }

    public void setDireccionID(Direccion direccionID) {
        this.direccionID = direccionID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idDenunciante != null ? idDenunciante.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Denunciante)) {
            return false;
        }
        Denunciante other = (Denunciante) object;
        if ((this.idDenunciante == null && other.idDenunciante != null) || (this.idDenunciante != null && !this.idDenunciante.equals(other.idDenunciante))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return this.nombre+" "+this.apellidoPaterno+" "+this.apellidoPaterno;
    }
    
    public void encrypt() throws Exception{
        Dao d = new Dao();
        this.nombre = d.encrypt(this.nombre);
        this.apellidoPaterno = d.encrypt(this.apellidoPaterno);
        this.apellidoMaterno = d.encrypt(this.apellidoMaterno);
        this.rfc = d.encrypt(this.rfc);
        this.calle = d.encrypt(this.calle);
        if(this.numeroInterior!=null){
            this.numeroInterior = d.encrypt(this.numeroInterior);
        }
        this.numeroExterior = d.encrypt(this.numeroExterior);
    }
    
    public void decrypt() throws Exception {
        if(!this.decrypted){
            Dao d = new Dao();
            this.nombre = d.decrypt(this.nombre);
            this.apellidoPaterno = d.decrypt(this.apellidoPaterno);
            this.apellidoMaterno = d.decrypt(this.apellidoMaterno);
            this.rfc = d.decrypt(this.rfc);
            this.calle = d.decrypt(this.calle);
            if (this.numeroInterior != null) {
                this.numeroInterior = d.decrypt(this.numeroInterior);
            }
            this.direccionID.decrypt();
            this.numeroExterior = d.decrypt(this.numeroExterior);
            this.decrypted = true;
        }
    }
    
}
