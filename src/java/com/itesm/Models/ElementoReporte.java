/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itesm.Models;

import com.itesm.Dao.Dao;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author jacobotapia
 */
@Entity
@Table(name = "ElementoReporte")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ElementoReporte.findAll", query = "SELECT e FROM ElementoReporte e")
    , @NamedQuery(name = "ElementoReporte.findByIdElementoReporte", query = "SELECT e FROM ElementoReporte e WHERE e.idElementoReporte = :idElementoReporte")
    , @NamedQuery(name = "ElementoReporte.findByContesto", query = "SELECT e FROM ElementoReporte e WHERE e.contesto = :contesto")})
public class ElementoReporte implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idElementoReporte")
    private Integer idElementoReporte;
    @Lob
    @Size(max = 65535)
    @Column(name = "parteInformativo")
    private String parteInformativo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "contesto")
    private boolean contesto;
    @JoinColumn(name = "reporteID", referencedColumnName = "idReporte")
    @ManyToOne(optional = false)
    private Reporte reporteID;
    @JoinColumn(name = "elementoID", referencedColumnName = "idElemento")
    @ManyToOne(optional = false)
    private Elemento elementoID;
    @JoinColumn(name = "patrullaID", referencedColumnName = "idPatrulla")
    @ManyToOne(optional = false)
    private Patrulla patrullaID;
    @Transient
    private boolean decrypted;

    public ElementoReporte() {
        this.elementoID = new Elemento();
        this.patrullaID = new Patrulla();
        this.decrypted = false;
    }

    public ElementoReporte(Integer idElementoReporte) {
        this.idElementoReporte = idElementoReporte;
    }

    public ElementoReporte(Integer idElementoReporte, boolean contesto) {
        this.idElementoReporte = idElementoReporte;
        this.contesto = contesto;
    }

    public Integer getIdElementoReporte() {
        return idElementoReporte;
    }

    public void setIdElementoReporte(Integer idElementoReporte) {
        this.idElementoReporte = idElementoReporte;
    }

    public String getParteInformativo() {
        return parteInformativo;
    }

    public void setParteInformativo(String parteInformativo) {
        this.parteInformativo = parteInformativo;
    }

    public boolean getContesto() {
        return contesto;
    }

    public void setContesto(boolean contesto) {
        this.contesto = contesto;
    }

    public Reporte getReporteID() {
        return reporteID;
    }

    public void setReporteID(Reporte reporteID) {
        this.reporteID = reporteID;
    }

    public Elemento getElementoID() {
        return elementoID;
    }

    public void setElementoID(Elemento elementoID) {
        this.elementoID = elementoID;
    }

    public Patrulla getPatrullaID() {
        return patrullaID;
    }

    public void setPatrullaID(Patrulla patrullaID) {
        this.patrullaID = patrullaID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idElementoReporte != null ? idElementoReporte.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ElementoReporte)) {
            return false;
        }
        ElementoReporte other = (ElementoReporte) object;
        if ((this.idElementoReporte == null && other.idElementoReporte != null) || (this.idElementoReporte != null && !this.idElementoReporte.equals(other.idElementoReporte))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.itesm.Models.ElementoReporte[ idElementoReporte=" + idElementoReporte + " ]";
    }
    
    public void encrypt() throws Exception{
        if(this.parteInformativo != null){
            Dao d = new Dao();
            this.parteInformativo = d.encrypt(this.parteInformativo);
        }
    }
    
    public void decrypt() throws Exception{
        if(!this.decrypted && this.parteInformativo!=null){
            Dao d =  new Dao();
            this.parteInformativo = d.decrypt(this.parteInformativo);
            this.decrypted = true;
        }
    }
    
}
