/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itesm.Ejbs;

import com.itesm.Models.Denunciante;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author jacobotapia
 */
@Local
public interface DenuncianteFacadeLocal {

    void create(Denunciante denunciante);

    void edit(Denunciante denunciante);

    void remove(Denunciante denunciante);

    Denunciante find(Object id);

    List<Denunciante> findAll();

    List<Denunciante> findRange(int[] range);

    int count();
    
}
