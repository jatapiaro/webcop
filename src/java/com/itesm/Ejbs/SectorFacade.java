/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itesm.Ejbs;

import com.itesm.Models.Elemento;
import com.itesm.Models.Sector;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author jacobotapia
 */
@Stateless
public class SectorFacade extends AbstractFacade<Sector> implements SectorFacadeLocal {

    @PersistenceContext(unitName = "WebCopPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public SectorFacade() {
        super(Sector.class);
    }

    @Override
    public Sector findByIdentificador(int identificador) {
        try{
            Query q = em.createQuery("SELECT s FROM Sector s WHERE s.identificador = :i AND s.activo = :a", Sector.class);
            q.setParameter("a", true);
            q.setParameter("i", identificador);
            return (Sector) q.getSingleResult();
        }catch(NoResultException e){
            return null;
        }
    }

    @Override
    public void delete(Integer id) {
        Query q = em.createQuery("Update Sector s SET s.activo = :a WHERE s.idSector = :id");
        q.setParameter("a", false);
        q.setParameter("id", id);
        q.executeUpdate();
    }

    @Override
    public List<Sector> findAllActive() {
        Query q =  em.createQuery("SELECT s FROM Sector s WHERE s.activo = :a ORDER BY s.identificador ASC", Sector.class);
        q.setParameter("a", true);
        return q.getResultList();
    }
    
    
}
