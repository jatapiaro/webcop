/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itesm.Ejbs;

import com.itesm.Models.Video;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author jacobotapia
 */
@Local
public interface VideoFacadeLocal {

    void create(Video video);

    void edit(Video video);

    void remove(Video video);

    Video find(Object id);

    List<Video> findAll();

    List<Video> findRange(int[] range);

    int count();
    
}
