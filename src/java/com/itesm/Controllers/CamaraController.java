/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itesm.Controllers;

import com.itesm.Ejbs.CamaraFacadeLocal;
import com.itesm.Ejbs.DireccionFacadeLocal;
import com.itesm.Models.Camara;
import com.itesm.Models.Direccion;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.primefaces.context.RequestContext;

/**
 *
 * @author jacobotapia
 */
@Named
@ViewScoped
public class CamaraController implements Serializable{

    @EJB
    CamaraFacadeLocal camaraEJB;
    private Camara camara;
    private List<Camara> camaras;
    
    @EJB
    DireccionFacadeLocal direccionEJB;
    
    @PostConstruct
    public void init(){
        this.camara=new Camara();
        this.camaras=new ArrayList<Camara>();
    }
    
    public void editar(){
        this.camaraEJB.edit(camara);
        this.camara = null;
        this.camara = new Camara();
    }
    
    public void listar() throws Exception{
        this.camaras=this.camaraEJB.findAll();
        for(Camara c : camaras){
            c.getIdDireccion().decrypt();
        }
    }
    
    public void crear(){
        if(this.camaraEJB.findByIdentificador(this.camara.getIdentificador())==null){
            this.camara.setActivo(true);
            this.camaraEJB.create(camara);
            this.camara = null;
            this.camara = new Camara();
            RequestContext.getCurrentInstance().execute("PF('widgetDatos').hide();");
        }else{
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Esta cámara/identificador ya esta registrada", "Esta cámara/identificador ya esta registrada"));
        }
    }
    
    public void eliminar(Integer id){
        this.camaraEJB.delete(id);
    }

    /**
     * @return the camara
     */
    public Camara getCamara() {
        return camara;
    }

    /**
     * @param camara the camara to set
     */
    public void setCamara(Camara camara) {
        this.camara = camara;
    }

    /**
     * @return the camaras
     */
    public List<Camara> getCamaras() {
        return camaras;
    }

    /**
     * @param camaras the camaras to set
     */
    public void setCamaras(List<Camara> camaras) {
        this.camaras = camaras;
    }
    
    public void validarDireccion() throws Exception{
        Direccion d=this.direccionEJB.find(this.camara.getIdDireccion().getCodigoPostal());
        d.decrypt();
        this.camara.setIdDireccion(d);
    }
    
    public void findForEdit(Object id) throws Exception{
        this.camara = this.camaraEJB.find(id);
        this.camara.getIdDireccion().decrypt();
    }
    
}
