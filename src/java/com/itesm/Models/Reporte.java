/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itesm.Models;

import com.itesm.Dao.Dao;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author jacobotapia
 */
@Entity
@Table(name = "Reporte")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Reporte.findAll", query = "SELECT r FROM Reporte r")
    , @NamedQuery(name = "Reporte.findByIdReporte", query = "SELECT r FROM Reporte r WHERE r.idReporte = :idReporte")
    , @NamedQuery(name = "Reporte.findByFechaHora", query = "SELECT r FROM Reporte r WHERE r.fechaHora = :fechaHora")})
public class Reporte implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idReporte")
    private Integer idReporte;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "titulo")
    private String titulo;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "hecho")
    private String hecho;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "folio")
    private String folio;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fechaHora")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaHora;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "calle")
    private String calle;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "reporteID")
    private List<Foto> fotoList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "reporteID")
    private List<InfractorReporte> infractorReporteList;
    @JoinColumn(name = "sectorID", referencedColumnName = "idSector")
    @ManyToOne(optional = false)
    private Sector sectorID;
    @JoinColumn(name = "direccionID", referencedColumnName = "codigoPostal")
    @ManyToOne(optional = false)
    private Direccion direccionID;
    @JoinColumn(name = "denuncianteID", referencedColumnName = "idDenunciante")
    @ManyToOne(optional = false)
    private Denunciante denuncianteID;
    @JoinColumn(name = "camaraID", referencedColumnName = "idCamara")
    @ManyToOne(optional = false)
    private Camara camaraID;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "reporteID")
    private List<Objeto> objetoList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "reporteID")
    private List<ElementoReporte> elementoReporteList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "reporteID")
    private List<Testigo> testigoList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "reporteID")
    private List<Video> videoList;

    public Reporte() {
        this.sectorID = new Sector();
        this.camaraID = new Camara();
        this.denuncianteID = new Denunciante();
        this.direccionID = new Direccion();
        this.folio = UUID.randomUUID().toString();
    }

    public Reporte(Integer idReporte) {
        this.idReporte = idReporte;
    }

    public Reporte(Integer idReporte, String titulo, String hecho, String folio, Date fechaHora, String calle) {
        this.idReporte = idReporte;
        this.titulo = titulo;
        this.hecho = hecho;
        this.folio = folio;
        this.fechaHora = fechaHora;
        this.calle = calle;
    }

    public Integer getIdReporte() {
        return idReporte;
    }

    public void setIdReporte(Integer idReporte) {
        this.idReporte = idReporte;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getHecho() {
        return hecho;
    }

    public void setHecho(String hecho) {
        this.hecho = hecho;
    }

    public String getFolio() {
        return folio;
    }

    public void setFolio(String folio) {
        this.folio = folio;
    }

    public Date getFechaHora() {
        return fechaHora;
    }

    public void setFechaHora(Date fechaHora) {
        this.fechaHora = fechaHora;
    }

    public String getCalle() {
        return calle;
    }

    public void setCalle(String calle) {
        this.calle = calle;
    }

    @XmlTransient
    public List<Foto> getFotoList() {
        return fotoList;
    }

    public void setFotoList(List<Foto> fotoList) {
        this.fotoList = fotoList;
    }

    @XmlTransient
    public List<InfractorReporte> getInfractorReporteList() {
        return infractorReporteList;
    }

    public void setInfractorReporteList(List<InfractorReporte> infractorReporteList) {
        this.infractorReporteList = infractorReporteList;
    }

    public Sector getSectorID() {
        return sectorID;
    }

    public void setSectorID(Sector sectorID) {
        this.sectorID = sectorID;
    }

    public Direccion getDireccionID() {
        return direccionID;
    }

    public void setDireccionID(Direccion direccionID) {
        this.direccionID = direccionID;
    }

    public Denunciante getDenuncianteID() {
        return denuncianteID;
    }

    public void setDenuncianteID(Denunciante denuncianteID) {
        this.denuncianteID = denuncianteID;
    }

    public Camara getCamaraID() {
        return camaraID;
    }

    public void setCamaraID(Camara camaraID) {
        this.camaraID = camaraID;
    }

    @XmlTransient
    public List<Objeto> getObjetoList() {
        return objetoList;
    }

    public void setObjetoList(List<Objeto> objetoList) {
        this.objetoList = objetoList;
    }

    @XmlTransient
    public List<ElementoReporte> getElementoReporteList() {
        return elementoReporteList;
    }

    public void setElementoReporteList(List<ElementoReporte> elementoReporteList) {
        this.elementoReporteList = elementoReporteList;
    }

    @XmlTransient
    public List<Testigo> getTestigoList() {
        return testigoList;
    }

    public void setTestigoList(List<Testigo> testigoList) {
        this.testigoList = testigoList;
    }

    @XmlTransient
    public List<Video> getVideoList() {
        return videoList;
    }

    public void setVideoList(List<Video> videoList) {
        this.videoList = videoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idReporte != null ? idReporte.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Reporte)) {
            return false;
        }
        Reporte other = (Reporte) object;
        if ((this.idReporte == null && other.idReporte != null) || (this.idReporte != null && !this.idReporte.equals(other.idReporte))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.itesm.Models.Reporte[ idReporte=" + idReporte + " ]";
    }
    
    public void encrypt() throws Exception{
        Dao d=new Dao();
        this.folio = d.encrypt(this.folio);
        this.hecho = d.encrypt(this.hecho);
        this.titulo = d.encrypt(this.titulo);
        this.calle = d.encrypt(this.calle);
        this.denuncianteID.encrypt();
        
        if(this.elementoReporteList!=null){
            for(ElementoReporte er : this.elementoReporteList){
                er.getElementoID().encrypt();
                er.getPatrullaID().encrypt();
                er.encrypt();
            }
        }
        
        if(this.infractorReporteList!=null){
            for (InfractorReporte ir : this.infractorReporteList) {
                ir.encrypt();
            }
        }
        
        if(this.objetoList!=null){
            for(Objeto o : this.objetoList){
                o.encrypt();
            }
        }
        
        if (this.fotoList != null) {
            for (Foto f : this.fotoList) {
                f.encrypt();
            }
        }
        
        if(this.videoList != null){
            for(Video v : this.videoList){
                v.encrypt();
            }
        }
        
        if(this.testigoList != null){
            for(Testigo t : this.testigoList){
                t.encrypt();
            }
        }
        
        
        //this.denuncianteID.encrypt();
        
    }
    
    public void decrypt() throws Exception {
        Dao d = new Dao();
        this.folio = d.decrypt(this.folio);
        this.hecho = d.decrypt(this.hecho);
        this.titulo = d.decrypt(this.titulo);
        this.calle = d.decrypt(this.calle);
        this.denuncianteID.decrypt();
        this.direccionID.decrypt();
        for(ElementoReporte er : this.getElementoReporteList()){
            er.getElementoID().decrypt();
            er.getPatrullaID().decrypt();
            er.decrypt();
        }
        for(InfractorReporte ir : this.infractorReporteList){
            ir.decrypt();
        }
        for(Objeto o : this.objetoList){
            o.decrypt();
        }
        for(Foto f : this.fotoList){
            f.decrypt();
        }
        for(Video f : this.videoList){
            f.decrypt();
        }
        for(Testigo t : this.testigoList){
            t.decrypt();
        }
    }
    
    public void decryptList() throws Exception {
        Dao d = new Dao();
        this.folio = d.decrypt(this.folio);
        this.hecho = d.decrypt(this.hecho);
        this.titulo = d.decrypt(this.titulo);
        this.calle = d.decrypt(this.calle);
        //this.direccionID.decrypt();
    }
    
}
