/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itesm.Models;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author jacobotapia
 */
@Entity
@Table(name = "Camara")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Camara.findAll", query = "SELECT c FROM Camara c")
    , @NamedQuery(name = "Camara.findByIdCamara", query = "SELECT c FROM Camara c WHERE c.idCamara = :idCamara")
    , @NamedQuery(name = "Camara.findByIdentificador", query = "SELECT c FROM Camara c WHERE c.identificador = :identificador")
    , @NamedQuery(name = "Camara.findByActivo", query = "SELECT c FROM Camara c WHERE c.activo = :activo")})
public class Camara implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idCamara")
    private Integer idCamara;
    @Basic(optional = false)
    @NotNull
    @Column(name = "identificador")
    private int identificador;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "calle")
    private String calle;
    @Basic(optional = false)
    @NotNull
    @Column(name = "activo")
    private boolean activo;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "camaraID")
    private List<Reporte> reporteList;
    @JoinColumn(name = "idDireccion", referencedColumnName = "codigoPostal")
    @ManyToOne(optional = false)
    private Direccion idDireccion;

    public Camara() {
        this.idDireccion = new Direccion();
    }

    public Camara(Integer idCamara) {
        this.idCamara = idCamara;
    }

    public Camara(Integer idCamara, int identificador, String calle, boolean activo) {
        this.idCamara = idCamara;
        this.identificador = identificador;
        this.calle = calle;
        this.activo = activo;
    }

    public Integer getIdCamara() {
        return idCamara;
    }

    public void setIdCamara(Integer idCamara) {
        this.idCamara = idCamara;
    }

    public int getIdentificador() {
        return identificador;
    }

    public void setIdentificador(int identificador) {
        this.identificador = identificador;
    }

    public String getCalle() {
        return calle;
    }

    public void setCalle(String calle) {
        this.calle = calle;
    }

    public boolean getActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    @XmlTransient
    public List<Reporte> getReporteList() {
        return reporteList;
    }

    public void setReporteList(List<Reporte> reporteList) {
        this.reporteList = reporteList;
    }

    public Direccion getIdDireccion() throws Exception {
        //this.idDireccion.decrypt();
        return idDireccion;
    }

    public void setIdDireccion(Direccion idDireccion) {
        this.idDireccion = idDireccion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCamara != null ? idCamara.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Camara)) {
            return false;
        }
        Camara other = (Camara) object;
        if ((this.idCamara == null && other.idCamara != null) || (this.idCamara != null && !this.idCamara.equals(other.idCamara))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return ""+this.identificador;
    }
    
}
