/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itesm.Models;

import com.itesm.Dao.Dao;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author jacobotapia
 */
@Entity
@Table(name = "Infractor")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Infractor.findAll", query = "SELECT i FROM Infractor i")
    , @NamedQuery(name = "Infractor.findByIdInfractor", query = "SELECT i FROM Infractor i WHERE i.idInfractor = :idInfractor")
    , @NamedQuery(name = "Infractor.findByGenero", query = "SELECT i FROM Infractor i WHERE i.genero = :genero")
    , @NamedQuery(name = "Infractor.findByEtnia", query = "SELECT i FROM Infractor i WHERE i.etnia = :etnia")
    , @NamedQuery(name = "Infractor.findByNivelEducacion", query = "SELECT i FROM Infractor i WHERE i.nivelEducacion = :nivelEducacion")
    , @NamedQuery(name = "Infractor.findByReligion", query = "SELECT i FROM Infractor i WHERE i.religion = :religion")
    , @NamedQuery(name = "Infractor.findByFechaNacimiento", query = "SELECT i FROM Infractor i WHERE i.fechaNacimiento = :fechaNacimiento")})
public class Infractor implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idInfractor")
    private Integer idInfractor;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "rfc")
    private String rfc;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "nombre")
    private String nombre;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "apellidoPaterno")
    private String apellidoPaterno;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "apellidoMaterno")
    private String apellidoMaterno;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "genero")
    private String genero;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "etnia")
    private String etnia;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "nivelEducacion")
    private String nivelEducacion;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "religion")
    private String religion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fechaNacimiento")
    @Temporal(TemporalType.DATE)
    private Date fechaNacimiento;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "infractorID")
    private List<InfractorReporte> infractorReporteList;
    @Transient
    private boolean decrypted;

    public Infractor() {
        this.decrypted = false;
    }

    public Infractor(Integer idInfractor) {
        this.idInfractor = idInfractor;
    }

    public Infractor(Integer idInfractor, String rfc, String nombre, String apellidoPaterno, String apellidoMaterno, String genero, String etnia, String nivelEducacion, String religion, Date fechaNacimiento) {
        this.idInfractor = idInfractor;
        this.rfc = rfc;
        this.nombre = nombre;
        this.apellidoPaterno = apellidoPaterno;
        this.apellidoMaterno = apellidoMaterno;
        this.genero = genero;
        this.etnia = etnia;
        this.nivelEducacion = nivelEducacion;
        this.religion = religion;
        this.fechaNacimiento = fechaNacimiento;
    }

    public Integer getIdInfractor() {
        return idInfractor;
    }

    public void setIdInfractor(Integer idInfractor) {
        this.idInfractor = idInfractor;
    }

    public String getRfc() {
        return rfc;
    }

    public void setRfc(String rfc) {
        this.rfc = rfc;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidoPaterno() {
        return apellidoPaterno;
    }

    public void setApellidoPaterno(String apellidoPaterno) {
        this.apellidoPaterno = apellidoPaterno;
    }

    public String getApellidoMaterno() {
        return apellidoMaterno;
    }

    public void setApellidoMaterno(String apellidoMaterno) {
        this.apellidoMaterno = apellidoMaterno;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getEtnia() {
        return etnia;
    }

    public void setEtnia(String etnia) {
        this.etnia = etnia;
    }

    public String getNivelEducacion() {
        return nivelEducacion;
    }

    public void setNivelEducacion(String nivelEducacion) {
        this.nivelEducacion = nivelEducacion;
    }

    public String getReligion() {
        return religion;
    }

    public void setReligion(String religion) {
        this.religion = religion;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    @XmlTransient
    public List<InfractorReporte> getInfractorReporteList() {
        return infractorReporteList;
    }

    public void setInfractorReporteList(List<InfractorReporte> infractorReporteList) {
        this.infractorReporteList = infractorReporteList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idInfractor != null ? idInfractor.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Infractor)) {
            return false;
        }
        Infractor other = (Infractor) object;
        if ((this.idInfractor == null && other.idInfractor != null) || (this.idInfractor != null && !this.idInfractor.equals(other.idInfractor))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return this.rfc+" - "+this.nombre+" "+this.apellidoPaterno+" "+this.apellidoMaterno;
    }
    
    public int getEdad() {

        long ageInMillis = new Date().getYear() - this.fechaNacimiento.getYear();
        return (int) ageInMillis;

    }

    public void encrypt() throws Exception {
        Dao d = new Dao();
        this.nombre = d.encrypt(this.nombre);
        this.apellidoMaterno = d.encrypt(this.apellidoMaterno);
        this.apellidoPaterno = d.encrypt(this.apellidoPaterno);
        this.rfc = d.encrypt(this.rfc);
    }
    
    public void encryptRfc() throws Exception{
        Dao d = new Dao();
        this.rfc = d.encrypt(this.rfc);
    }

    public void decrypt() throws Exception {
        if(!this.isDecrypted()){
            Dao d = new Dao();
            this.nombre = d.decrypt(this.nombre);
            this.rfc = d.decrypt(this.rfc);
            this.apellidoMaterno = d.decrypt(this.apellidoMaterno);
            this.apellidoPaterno = d.decrypt(this.apellidoPaterno);
            this.setDecrypted(true);
        }
    }

    /**
     * @return the decrypted
     */
    public boolean isDecrypted() {
        return decrypted;
    }

    /**
     * @param decrypted the decrypted to set
     */
    public void setDecrypted(boolean decrypted) {
        this.decrypted = decrypted;
    }
    
    
}
