/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itesm.Ejbs;

import com.itesm.Models.Elemento;
import com.itesm.Models.Sector;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author jacobotapia
 */
@Stateless
public class ElementoFacade extends AbstractFacade<Elemento> implements ElementoFacadeLocal {

    @PersistenceContext(unitName = "WebCopPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ElementoFacade() {
        super(Elemento.class);
    }

    @Override
    public Elemento findByPlaca(String placa) {        
        Query q = em.createQuery("SELECT e FROM Elemento e WHERE e.placa = :p AND e.activo = :a", Elemento.class);
        q.setParameter("a", true);
        q.setParameter("p", placa);
        return (Elemento) q.getSingleResult();
    }
    
    @Override
    public Elemento findByPlaca2(String placa){
       try{
           Query q = em.createQuery("SELECT e FROM Elemento e WHERE e.placa = :p AND e.activo = :a", Elemento.class);
           q.setParameter("a", true);
           q.setParameter("p", placa);
           return (Elemento) q.getSingleResult();
       }catch(NoResultException e){
           return null;
       } 
    }

    @Override
    public Elemento login(String placa, String password) {
        Query q = em.createQuery("SELECT e FROM Elemento e WHERE e.placa = :p AND e.password = :pss AND e.activo = :a", Elemento.class);
        q.setParameter("a", true);
        q.setParameter("p", placa);
        q.setParameter("pss", password);
        return (Elemento) q.getSingleResult(); 
    }

    @Override
    public List<Elemento> findAllActivos() {
        Query q = em.createQuery("SELECT e FROM Elemento e WHERE e.activo = :a ORDER BY e.idElemento DESC", Elemento.class);
        q.setParameter("a", true);
        return q.getResultList();
    }

    @Override
    public void delete(Integer id) {
        Query q = em.createQuery("Update Elemento e SET e.activo = :a WHERE e.idElemento = :id");
        q.setParameter("a", false);
        q.setParameter("id", id);
        q.executeUpdate();
    }
    
}
