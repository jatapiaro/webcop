/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itesm.Ejbs;

import com.itesm.Models.Patrulla;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author jacobotapia
 */
@Local
public interface PatrullaFacadeLocal {

    void create(Patrulla patrulla);

    void edit(Patrulla patrulla);

    void remove(Patrulla patrulla);

    Patrulla find(Object id);

    List<Patrulla> findAll();
    
    List<Patrulla> findAllActivo();

    List<Patrulla> findRange(int[] range);

    int count();
    
    Patrulla findByPlacas(String placas);
    
    Patrulla findByPlacas2(String placas);
    
    void delete(Integer id);
    
    
}
