/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itesm.Ejbs;

import com.itesm.Models.Testigo;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author jacobotapia
 */
@Local
public interface TestigoFacadeLocal {

    void create(Testigo testigo);

    void edit(Testigo testigo);

    void remove(Testigo testigo);

    Testigo find(Object id);

    List<Testigo> findAll();

    List<Testigo> findRange(int[] range);

    int count();
    
}
