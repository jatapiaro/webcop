/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itesm.Controllers;

import com.itesm.Dao.Dao;
import com.itesm.Ejbs.DenuncianteFacadeLocal;
import com.itesm.Ejbs.DireccionFacadeLocal;
import com.itesm.Ejbs.ElementoFacadeLocal;
import com.itesm.Ejbs.ElementoReporteFacadeLocal;
import com.itesm.Ejbs.FotoFacadeLocal;
import com.itesm.Ejbs.InfractorFacadeLocal;
import com.itesm.Ejbs.InfractorReporteFacadeLocal;
import com.itesm.Ejbs.ObjetoFacadeLocal;
import com.itesm.Ejbs.PatrullaFacadeLocal;
import com.itesm.Ejbs.ReporteFacadeLocal;
import com.itesm.Ejbs.TestigoFacadeLocal;
import com.itesm.Ejbs.VideoFacadeLocal;
import com.itesm.Helpers.UploadImageHelper;
import com.itesm.Helpers.UploadVideoHelper;
import com.itesm.Models.Direccion;
import com.itesm.Models.Elemento;
import com.itesm.Models.ElementoReporte;
import com.itesm.Models.Foto;
import com.itesm.Models.Infractor;
import com.itesm.Models.InfractorReporte;
import com.itesm.Models.Objeto;
import com.itesm.Models.Patrulla;
import com.itesm.Models.Reporte;
import com.itesm.Models.Testigo;
import com.itesm.Models.Video;
import java.io.IOException;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import javax.servlet.http.Part;
import org.primefaces.context.RequestContext;

/**
 *
 * @author jacobotapia
 */
@Named
@ViewScoped
public class ReporteController implements Serializable{


    
    @EJB
    private ReporteFacadeLocal reporteEJB;
    private Reporte reporte;
    private List<Reporte> reportes;
    private ElementoReporte elementoReporte;
    private InfractorReporte infractorReporte;
    private List<String> delitos;
    private Objeto objeto;
    private Foto foto;
    private boolean uploaded = false;
    private Video video;
    private String videoUrl;
    private Testigo testigo;

    @EJB
    private DireccionFacadeLocal direccionEJB;
    
    @EJB
    private DenuncianteFacadeLocal denuncianteEJB;
    
    @EJB
    private ElementoFacadeLocal elementoEJB;
    
    @EJB
    private PatrullaFacadeLocal patrullaEJB;
    
    @EJB
    private ElementoReporteFacadeLocal elementoReporteEJB;
    
    @EJB
    private InfractorReporteFacadeLocal infractorReporteEJB;
    
    @EJB
    private InfractorFacadeLocal infractorEJB;
    
    @EJB
    private ObjetoFacadeLocal objetoEJB;
    
    @EJB
    private FotoFacadeLocal fotoEJB;
    
    @EJB
    private VideoFacadeLocal videoEJB;
    
    @EJB
    private TestigoFacadeLocal testigoEJB;
    
    @PostConstruct
    public void init() {
        this.reporte = new Reporte();
        this.reportes = new ArrayList<Reporte>();
        this.elementoReporte = new ElementoReporte();
        this.infractorReporte = new InfractorReporte();
        this.objeto = new Objeto();
        this.foto = new Foto();
        this.video = new Video();
        this.videoUrl = "";
        this.testigo = new Testigo();
        this.setDelitos(new ArrayList<String>());
        setDelitos(new ArrayList<String>());
        getDelitos().add("Homicidio Doloso");
        getDelitos().add("Homicidio Imprudente");
        getDelitos().add("Asesinato");
        getDelitos().add("Auxilio e inducción al suicidio");
        getDelitos().add("Aborto doloso");
        getDelitos().add("Aborto imprudente");
        getDelitos().add("Concepto de lesión");
        getDelitos().add("Consentimiento en las lesiones");
        getDelitos().add("Maltrato Singular");
        getDelitos().add("Particiàción en riña tumultaria");
        getDelitos().add("Tráfico ilegal de órganos");
        getDelitos().add("Detenciones ilegales y secuestros");
        getDelitos().add("Delitos de amenazas");
        getDelitos().add("Delitos de coacciones");
        getDelitos().add("Tortura y otros delitos contra la integridad moral");
        getDelitos().add("Trata de seres humanos");
        getDelitos().add("Agresiones sexuales");
        getDelitos().add("Abusos sexuales");
        getDelitos().add("Agresiones y abusos a menores de 13 años");
        getDelitos().add("Acoso sexual");
        getDelitos().add("Exhibiciones y provocación sexual");
        getDelitos().add("Allanamiento de morada");
        getDelitos().add("Robo");
        getDelitos().add("Extorsion");
        getDelitos().add("Estafa");
        getDelitos().add("Daños");
        getDelitos().add("Alzamiento de bienes");
        getDelitos().add("Tráfico de drogas");
        getDelitos().add("Conducir a exceso de velocidad");
        getDelitos().add("Conducir bajo influencia de tóxicos");
        getDelitos().add("Abandono de familia");
        getDelitos().add("Explotación de menores");
        getDelitos().add("Incumplimiento de deberes económicos");
        getDelitos().add("Falsificación de moneda");
        getDelitos().add("Tráfico de influencias");
        getDelitos().add("Encubrimiento");
        java.util.Collections.sort(getDelitos());
    }
    
    public void listar() throws Exception{
        this.reportes=this.reporteEJB.findAll();
        for(Reporte r : reportes){
            r.decryptList();
        }
    }
    
    public void crear() throws Exception{
        this.reporte.getDenuncianteID().setRfc(this.reporte.getDenuncianteID().getRfc().toUpperCase());
        this.reporte.encrypt();
        this.denuncianteEJB.create(this.reporte.getDenuncianteID());
        this.reporteEJB.create(reporte);
        this.redirectToViewPage(this.reporte.getIdReporte());
    }

    /**
     * @return the reporte
     */
    public Reporte getReporte() {
        return reporte;
    }

    /**
     * @param reporte the reporte to set
     */
    public void setReporte(Reporte reporte) {
        this.reporte = reporte;
    }

    /**
     * @return the reportes
     */
    public List<Reporte> getReportes() {
        return reportes;
    }

    /**
     * @param reportes the reportes to set
     */
    public void setReportes(List<Reporte> reportes) {
        this.reportes = reportes;
    }
    
    public void validarDireccion() throws Exception {
        Direccion d = this.direccionEJB.find(this.reporte.getDireccionID().getCodigoPostal());
        d.decrypt();
        this.reporte.setDireccionID(d);
    }
    
    public void validarDireccion2() throws Exception {
        Direccion d = this.direccionEJB.find(this.reporte.getDenuncianteID().getDireccionID().getCodigoPostal());
        d.decrypt();
        this.reporte.getDenuncianteID().setDireccionID(d);
    }
    
    public Date currentMinDate() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        date.setHours(0);
        date.setMinutes(0);
        date.setSeconds(0);
        return date;
    }

    public Date currentMaxDate() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        date.setHours(23);
        date.setMinutes(59);
        date.setSeconds(59);
        return date;
    }
    
    public void loadReporte() throws Exception{
        try{
            ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
            Map<String, Object> sessionMap = externalContext.getSessionMap();
            Object id = sessionMap.get("id");
            this.reporte = this.reporteEJB.find(id);
            this.reporte.decrypt();
        }catch(Exception e){
            throw e;
        }
    }
    
    public void redirectToViewPage(Object id) throws IOException{
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        Map<String, Object> sessionMap = externalContext.getSessionMap();
        sessionMap.put("id", id);
        String contextPath = FacesContext.getCurrentInstance().getExternalContext().getApplicationContextPath();
        FacesContext.getCurrentInstance().getExternalContext().redirect(contextPath + "/faces/views/reporte/View.xhtml");
    }
    
    public void redirectToCreatePage() throws IOException{
        String contextPath = FacesContext.getCurrentInstance().getExternalContext().getApplicationContextPath();
        FacesContext.getCurrentInstance().getExternalContext().redirect(contextPath + "/faces/views/reporte/Create.xhtml");
    }

    /**
     * @return the elementoReporte
     */
    public ElementoReporte getElementoReporte() {
        return elementoReporte;
    }

    /**
     * @param elementoReporte the elementoReporte to set
     */
    public void setElementoReporte(ElementoReporte elementoReporte) {
        this.elementoReporte = elementoReporte;
    }
    
    public void aniadirElementoReporte() throws Exception{
        
        if(this.elementoEstaRegistrado(this.elementoReporte.getElementoID().getPlaca())){
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Este elemento ya esta en el reporte", "Este elemento ya esta en el reporte"));
        }else{
            this.reporte.encrypt();
            this.elementoReporte.setContesto(false);
            this.elementoReporte.setReporteID(this.reporte);
            this.reporte.getElementoReporteList().add(elementoReporte);
            this.elementoReporteEJB.create(elementoReporte);
            this.reporteEJB.edit(reporte);
            this.elementoReporte = null;
            this.elementoReporte = new ElementoReporte(); 
            RequestContext.getCurrentInstance().execute("PF('widgetElementos').hide();");
        }
    }
    
    public void buscarElemento() throws Exception{
        String placa = this.elementoReporte.getElementoID().getPlaca();
        Elemento e = this.elementoEJB.findByPlaca(placa);
        e.decrypt();
        this.elementoReporte.setElementoID(e);
    }
    
    public void buscarPatrulla() throws Exception {
        String placas = this.elementoReporte.getPatrullaID().getPlacas();
        Dao d = new Dao();
        placas = d.encrypt(placas);
        Patrulla p = this.patrullaEJB.findByPlacas(placas);
        p.decrypt();
        this.elementoReporte.setPatrullaID(p);
    }
    
    public void buscarElementoReporte(int id){
        for(ElementoReporte e : this.reporte.getElementoReporteList()){
            if(e.getIdElementoReporte().intValue() == id){
                this.elementoReporte = e;
                break;
            }
        }
    }
    
    public void agregarParteInformativo() throws Exception{
        this.elementoReporte.setContesto(true);
        this.elementoReporte.encrypt();
        this.elementoReporteEJB.edit(elementoReporte);
        this.elementoReporte = null;
        this.elementoReporte = new ElementoReporte();
    }

    /**
     * @return the infractorReporte
     */
    public InfractorReporte getInfractorReporte() {
        return infractorReporte;
    }

    /**
     * @param infractorReporte the infractorReporte to set
     */
    public void setInfractorReporte(InfractorReporte infractorReporte) {
        this.infractorReporte = infractorReporte;
    }
    
    public void buscarInfractor() throws Exception{
        Infractor f = this.infractorReporte.getInfractorID();
        f.encryptRfc();
        f = this.infractorEJB.findByRfc(f.getRfc());
        f.decrypt();
        this.infractorReporte.setInfractorID(f);
        System.out.println(f);
    }
    
    public void validarDireccion3() throws Exception {
        Direccion d = this.direccionEJB.find(this.infractorReporte.getDireccionID().getCodigoPostal());
        d.decrypt();
        this.infractorReporte.setDireccionID(d);
    }
    
    public void validarDireccion4() throws Exception {
        Direccion d = this.direccionEJB.find(this.testigo.getDireccionID().getCodigoPostal());
        d.decrypt();
        this.testigo.setDireccionID(d);
    }

    /**
     * @return the delitos
     */
    public List<String> getDelitos() {
        return delitos;
    }

    /**
     * @param delitos the delitos to set
     */
    public void setDelitos(List<String> delitos) {
        this.delitos = delitos;
    }
    
    public void agregarInfractorReporte() throws Exception{
        this.reporte.encrypt();
        this.infractorReporte.setReporteID(reporte);
        this.infractorReporte.makeDelitosString();
        this.infractorReporte.encrypt();
        this.reporte.getInfractorReporteList().add(infractorReporte);
        this.infractorReporteEJB.create(infractorReporte);
        this.reporteEJB.edit(reporte);
        this.infractorReporte =  null;
        this.infractorReporte = new InfractorReporte();
    }
    
    public void encontrarDetenidoParaFoto(Object id) throws Exception{
        this.uploaded = false;
        this.infractorReporte = this.infractorReporteEJB.find(id);
        this.infractorReporte.decrypt();
    }
    
    public void resetUploaded(){
        this.uploaded = false;
    }

    public void subirImagenDetenido() throws Exception{
        if(this.uploaded == false && this.infractorReporte.getFile()!=null){
            UploadImageHelper u = new UploadImageHelper();
            if(this.infractorReporte.getHasImage()){
                u.deleteFile(this.infractorReporte.getImagen());
            }
            String s = u.processUpload(this.infractorReporte.getFile());
            this.uploaded = true;
            this.infractorReporte.setHasImage(true);
            this.infractorReporte.setImagen(s);
            this.infractorReporte.encrypt();
            this.infractorReporteEJB.edit(infractorReporte);
            this.infractorReporte = null;
            this.infractorReporte = new InfractorReporte();
        }
    }
    
    
    public void agregarObjeto() throws Exception{
        this.reporte.encrypt();
        this.objeto.setReporteID(reporte);
        this.getObjeto().encrypt();
        this.reporte.getObjetoList().add(getObjeto());
        this.objetoEJB.create(getObjeto());
        this.reporteEJB.edit(reporte);
        this.setObjeto(null);
        this.setObjeto(new Objeto());
    }
    
    public void encontrarObjetoParaFoto(Object id) throws Exception {
        this.uploaded = false;
        if(id!=null){
            this.objeto = this.objetoEJB.find(id);
            this.objeto.decrypt();
        }
    }

    public void subirImagenObjeto() throws Exception {
        if (this.uploaded == false && this.objeto.getFile() != null) {
            UploadImageHelper u = new UploadImageHelper();
            if (this.objeto.getHasImage()) {
                u.deleteFile(this.objeto.getImagen());
            }
            String s = u.processUpload(this.objeto.getFile());
            this.uploaded = true;
            this.objeto.setHasImage(true);
            this.objeto.setImagen(s);
            this.objeto.encrypt();
            this.objetoEJB.edit(objeto);
            this.objeto = null;
            this.objeto = new Objeto();
        }
    }
    
    public void agregarFoto() throws Exception{
        if(this.uploaded == false && this.foto.getFile()!=null){
            UploadImageHelper u = new UploadImageHelper();
            String s = u.processUpload(this.foto.getFile());
            this.foto.setFoto(s);
            this.reporte.encrypt();
            this.foto.setReporteID(reporte);
            this.foto.encrypt();
            this.reporte.getFotoList().add(foto);
            this.fotoEJB.create(foto);
            this.reporteEJB.edit(reporte);
            this.foto = null;
            this.foto = new Foto();
            this.uploaded = true;
        }
    }
    
    /**
     * @return the foto
     */
    public Foto getFoto() {
        return foto;
    }

    /**
     * @param foto the foto to set
     */
    public void setFoto(Foto foto) {
        this.foto = foto;
    }

    /**
     * @return the objeto
     */
    public Objeto getObjeto() {
        return objeto;
    }

    /**
     * @param objeto the objeto to set
     */
    public void setObjeto(Objeto objeto) {
        this.objeto = objeto;
    }

    /**
     * @return the video
     */
    public Video getVideo() {
        return video;
    }

    /**
     * @param video the video to set
     */
    public void setVideo(Video video) {
        this.video = video;
    }
    
    public void agregarVideo() throws Exception {
        if (this.uploaded == false && this.video.getFile() != null) {
            UploadVideoHelper u = new UploadVideoHelper();
            String s = u.processUpload(this.video.getFile());
            this.video.setVideo(s);
            this.reporte.encrypt();
            this.video.setReporteID(reporte);
            this.video.encrypt();
            this.reporte.getVideoList().add(video);
            this.videoEJB.create(video);
            this.reporteEJB.edit(reporte);
            this.video = null;
            this.video = new Video();
            this.video.setNombre("");
            this.video.setDescripcion("");
            this.uploaded = true;
        }
    }

    /**
     * @return the videoUrl
     */
    public String getVideoUrl() {
        return videoUrl;
    }

    /**
     * @param videoUrl the videoUrl to set
     */
    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }
    
    public void loadVideo(String url){
        this.videoUrl = url;
    }

    /**
     * @return the testigo
     */
    public Testigo getTestigo() {
        return testigo;
    }

    /**
     * @param testigo the testigo to set
     */
    public void setTestigo(Testigo testigo) {
        this.testigo = testigo;
    }
    
    public void agregarTestigo() throws Exception{
        this.testigo.setRfc(this.testigo.getRfc().toUpperCase());
        this.reporte.encrypt();
        this.testigo.setReporteID(reporte);
        this.testigo.encrypt();
        this.testigoEJB.create(testigo);
        this.reporte.getTestigoList().add(testigo);  
        this.reporteEJB.edit(reporte);
        this.testigo = null;
        this.testigo = new Testigo();
    }
    
    
    public boolean elementoEstaRegistrado(String placa){
        for(ElementoReporte er : this.reporte.getElementoReporteList()){
            if(er.getElementoID().getPlaca().equals(placa)){
                return true;
            }
        }
        return false;
    }
    
    public void testigoRegistrado(){
        String rfc = this.testigo.getRfc().toUpperCase();
        for(Testigo t : this.reporte.getTestigoList()){
            if(t.getRfc().equals(rfc)){
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "El testigo con este RFC ya esta registrado", "El testigo con este RFC ya esta registrado"));
                return;
            }
        }
    }

  
    
}
