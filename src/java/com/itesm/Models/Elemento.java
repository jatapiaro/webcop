/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itesm.Models;

import com.itesm.Dao.Dao;
import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author jacobotapia
 */
@Entity
@Table(name = "Elemento")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Elemento.findAll", query = "SELECT e FROM Elemento e")
    , @NamedQuery(name = "Elemento.findByIdElemento", query = "SELECT e FROM Elemento e WHERE e.idElemento = :idElemento")
    , @NamedQuery(name = "Elemento.findByNivelAcceso", query = "SELECT e FROM Elemento e WHERE e.nivelAcceso = :nivelAcceso")
    , @NamedQuery(name = "Elemento.findByActivo", query = "SELECT e FROM Elemento e WHERE e.activo = :activo")})
public class Elemento implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idElemento")
    private Integer idElemento;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "placa")
    private String placa;
    @Basic(optional = false)
    @NotNull
    @Column(name = "nivelAcceso")
    private short nivelAcceso;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "nombre")
    private String nombre;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "apellidoPaterno")
    private String apellidoPaterno;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "apellidoMaterno")
    private String apellidoMaterno;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "password")
    private String password;
    @Basic(optional = false)
    @NotNull
    @Column(name = "activo")
    private boolean activo;
    @JoinColumn(name = "sectorID", referencedColumnName = "idSector")
    @ManyToOne(optional = false)
    private Sector sectorID;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "elementoID")
    private List<ElementoReporte> elementoReporteList;
    @Transient 
    private boolean decrypted;

    public Elemento() {
        this.decrypted = false;
        this.sectorID = new Sector(); 
    }

    public Elemento(Integer idElemento) {
        this.idElemento = idElemento;
    }

    public Elemento(Integer idElemento, String placa, short nivelAcceso, String nombre, String apellidoPaterno, String apellidoMaterno, String password, boolean activo) {
        this.idElemento = idElemento;
        this.placa = placa;
        this.nivelAcceso = nivelAcceso;
        this.nombre = nombre;
        this.apellidoPaterno = apellidoPaterno;
        this.apellidoMaterno = apellidoMaterno;
        this.password = password;
        this.activo = activo;
    }

    public Integer getIdElemento() {
        return idElemento;
    }

    public void setIdElemento(Integer idElemento) {
        this.idElemento = idElemento;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public short getNivelAcceso() {
        return nivelAcceso;
    }

    public void setNivelAcceso(short nivelAcceso) {
        this.nivelAcceso = nivelAcceso;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidoPaterno() {
        return apellidoPaterno;
    }

    public void setApellidoPaterno(String apellidoPaterno) {
        this.apellidoPaterno = apellidoPaterno;
    }

    public String getApellidoMaterno() {
        return apellidoMaterno;
    }

    public void setApellidoMaterno(String apellidoMaterno) {
        this.apellidoMaterno = apellidoMaterno;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean getActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    public Sector getSectorID() {
        return sectorID;
    }

    public void setSectorID(Sector sectorID) {
        this.sectorID = sectorID;
    }

    @XmlTransient
    public List<ElementoReporte> getElementoReporteList() {
        return elementoReporteList;
    }

    public void setElementoReporteList(List<ElementoReporte> elementoReporteList) {
        this.elementoReporteList = elementoReporteList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idElemento != null ? idElemento.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Elemento)) {
            return false;
        }
        Elemento other = (Elemento) object;
        if ((this.idElemento == null && other.idElemento != null) || (this.idElemento != null && !this.idElemento.equals(other.idElemento))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        String s = this.placa+" - "+this.nombreCompleto();
        if(s.equals("null - null null null")){
            return "";
        }else{
            return s;
        }
    }
    
    public void encrypt() throws Exception {
        Dao d = new Dao();
        this.nombre = d.encrypt(this.nombre);
        this.apellidoPaterno = d.encrypt(this.apellidoPaterno);
        this.apellidoMaterno = d.encrypt(this.apellidoMaterno);
        this.password = d.encrypt(this.password);
    }
    
    public void encrypt2() throws Exception{
        Dao d = new Dao();
        this.nombre = d.encrypt(this.nombre);
        this.apellidoPaterno = d.encrypt(this.apellidoPaterno);
        this.apellidoMaterno = d.encrypt(this.apellidoMaterno);  
    }

    public void decrypt() throws Exception {
        if(!this.decrypted){
            Dao d = new Dao();
            this.nombre = d.decrypt(this.nombre);
            this.apellidoPaterno = d.decrypt(this.apellidoPaterno);
            this.apellidoMaterno = d.decrypt(this.apellidoMaterno);
            this.decrypted = true;
        }
    }
    
    public String nombreCompleto(){
        return this.nombre+" "+this.apellidoPaterno+" "+this.apellidoMaterno;
    }
    
    public String nivelDeAccesoString(){
        if(this.nivelAcceso == 0){
            return "Policia";
        }else if(this.nivelAcceso == 1){
            return "Personal del Ministerio Público";
        }else{
            return "Administrador";
        }
    }
    
}
