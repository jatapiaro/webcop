/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itesm.Controllers;

import com.itesm.Ejbs.SectorFacadeLocal;
import com.itesm.Models.Sector;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.primefaces.context.RequestContext;

/**
 *
 * @author jacobotapia
 */
@Named
@ViewScoped
public class SectorController implements Serializable{
    
    @EJB
    SectorFacadeLocal sectorEJB;
    private Sector sector;
    private List<Sector> sectores;
    
    @PostConstruct
    public void init(){
        sector = new Sector();
        sectores = new ArrayList<Sector>();
    }
    
    public void listar(){
        this.sectores=this.sectorEJB.findAllActive();
    }
    
    public void eliminar(Integer id){
        this.sectorEJB.delete(id);
    }
    
    public void crear(){
        if(this.sectorEJB.findByIdentificador(this.sector.getIdentificador())==null){
            this.sector.setActivo(true);
            this.sectorEJB.create(sector);
            this.sector = null;
            this.sector = new Sector();
            RequestContext.getCurrentInstance().execute("PF('widgetDatos').hide();");
        }else{
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Este sector ya esta registrado", "Este sector ya esta registrado"));
        }
    }
    
    public void editar(){
        this.sectorEJB.edit(sector);
        this.sector = null;
        this.sector = new Sector();
    }
    
    public void findForEdit(Object id){
        this.sector = this.sectorEJB.find(id);
    }

    /**
     * @return the sector
     */
    public Sector getSector() {
        return sector;
    }

    /**
     * @param sector the sector to set
     */
    public void setSector(Sector sector) {
        this.sector = sector;
    }

    /**
     * @return the sectores
     */
    public List<Sector> getSectores() {
        return sectores;
    }

    /**
     * @param sectores the sectores to set
     */
    public void setSectores(List<Sector> sectores) {
        this.sectores = sectores;
    }
    
}
