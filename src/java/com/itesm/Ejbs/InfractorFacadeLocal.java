/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itesm.Ejbs;

import com.itesm.Models.Infractor;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author jacobotapia
 */
@Local
public interface InfractorFacadeLocal {

    void create(Infractor infractor);

    void edit(Infractor infractor);

    void remove(Infractor infractor);

    Infractor find(Object id);

    List<Infractor> findAll();

    List<Infractor> findRange(int[] range);

    int count();
    
    Infractor findByRfc(String rfc);
    
    Infractor findByRfc2(String rfc);
}
