/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itesm.Ejbs;

import com.itesm.Models.ElementoReporte;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author jacobotapia
 */
@Local
public interface ElementoReporteFacadeLocal {

    void create(ElementoReporte elementoReporte);

    void edit(ElementoReporte elementoReporte);

    void remove(ElementoReporte elementoReporte);

    ElementoReporte find(Object id);

    List<ElementoReporte> findAll();

    List<ElementoReporte> findRange(int[] range);

    int count();
    
}
