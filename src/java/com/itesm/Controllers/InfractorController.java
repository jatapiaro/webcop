/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itesm.Controllers;

import com.itesm.Dao.Dao;
import com.itesm.Ejbs.InfractorFacadeLocal;
import com.itesm.Models.Infractor;
import com.itesm.Models.InfractorReporte;
import com.itesm.Models.Reporte;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.primefaces.context.RequestContext;

/**
 *
 * @author jacobotapia
 */

@Named
@ViewScoped
public class InfractorController implements Serializable{
    
    @EJB
    private InfractorFacadeLocal infractorEJB;
    private Infractor infractor;
    private List<Infractor> infractores;
    private List<Reporte> reportesInfractor;
    
    @PostConstruct
    public void init(){
        infractor=new Infractor();
        infractores=new ArrayList<Infractor>();
        reportesInfractor = new ArrayList<Reporte>();
    }
    
    public void registrar() throws Exception{
        Dao d = new Dao();
        String rfc = d.encrypt(this.infractor.getRfc().toUpperCase());
        if(this.infractorEJB.findByRfc2(rfc)==null){
            this.infractor.setRfc(this.infractor.getRfc().toUpperCase());
            this.infractor.encrypt();
            this.infractorEJB.create(infractor);
            this.infractor = null;
            this.infractor = new Infractor();
            RequestContext.getCurrentInstance().execute("PF('widgetDatos').hide();");
        }else{
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Este infractor ya estña registrado", "Este infractor ya estña registrado"));
        }
    }
    
    public void listar() throws Exception{
        this.infractores=this.infractorEJB.findAll();
        for(Infractor f:infractores){
            f.decrypt();
        }
    }
    
    public void modificar() throws Exception{
        this.infractor.encrypt();
        this.infractorEJB.edit(infractor);
        this.infractor = null;
        this.infractor = new Infractor();
    }
    
    public void findForEdit(Object id) throws Exception{
        this.infractor = this.infractorEJB.find(id);
        this.infractor.decrypt();
    }
    
    /**
     * @return the infractor
     */
    public Infractor getInfractor() {
        return infractor;
    }

    /**
     * @param infractor the infractor to set
     */
    public void setInfractor(Infractor infractor) {
        this.infractor = infractor;
    }

    /**
     * @return the infractores
     */
    public List<Infractor> getInfractores() {
        return infractores;
    }

    /**
     * @param infractores the infractores to set
     */
    public void setInfractores(List<Infractor> infractores) {
        this.infractores = infractores;
    }
    
    public void loadInfractor() throws Exception {
        try {
            ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
            Map<String, Object> sessionMap = externalContext.getSessionMap();
            Object id = sessionMap.get("idInfractor");
            this.infractor = this.infractorEJB.find(id);
            this.infractor.decrypt();
            for(InfractorReporte r : this.infractor.getInfractorReporteList()){
                r.getReporteID().decrypt();
                System.out.println(r.getReporteID());
                this.reportesInfractor.add(r.getReporteID());
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public void redirectToViewPage(Object id) throws IOException {
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        Map<String, Object> sessionMap = externalContext.getSessionMap();
        sessionMap.put("idInfractor", id);
        String contextPath = FacesContext.getCurrentInstance().getExternalContext().getApplicationContextPath();
        FacesContext.getCurrentInstance().getExternalContext().redirect(contextPath + "/faces/views/infractor/View.xhtml");
    }

    /**
     * @return the reportesInfractor
     */
    public List<Reporte> getReportesInfractor() {
        return reportesInfractor;
    }

    /**
     * @param reportesInfractor the reportesInfractor to set
     */
    public void setReportesInfractor(List<Reporte> reportesInfractor) {
        this.reportesInfractor = reportesInfractor;
    }
    
}
