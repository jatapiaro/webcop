/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itesm.Ejbs;

import com.itesm.Models.InfractorReporte;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author jacobotapia
 */
@Local
public interface InfractorReporteFacadeLocal {

    void create(InfractorReporte infractorReporte);

    void edit(InfractorReporte infractorReporte);

    void remove(InfractorReporte infractorReporte);

    InfractorReporte find(Object id);

    List<InfractorReporte> findAll();

    List<InfractorReporte> findRange(int[] range);

    int count();
    
}
