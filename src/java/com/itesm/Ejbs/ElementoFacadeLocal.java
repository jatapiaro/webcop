/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itesm.Ejbs;

import com.itesm.Models.Elemento;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author jacobotapia
 */
@Local
public interface ElementoFacadeLocal {

    void create(Elemento elemento);

    void edit(Elemento elemento);

    void remove(Elemento elemento);

    Elemento find(Object id);

    List<Elemento> findAll();

    List<Elemento> findRange(int[] range);

    int count();
    
    void delete(Integer id);
    
    List<Elemento> findAllActivos();
    
    Elemento findByPlaca(String placa);
    
    Elemento findByPlaca2(String placa);
    
    Elemento login(String placa,String password);
}
