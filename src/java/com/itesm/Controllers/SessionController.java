/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itesm.Controllers;

import com.itesm.Dao.Dao;
import com.itesm.Ejbs.ElementoFacadeLocal;
import com.itesm.Models.Elemento;
import java.io.IOException;
import java.io.Serializable;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author jacobotapia
 */
@ManagedBean
@SessionScoped
public class SessionController implements Serializable {

    private Elemento elemento = null;
    private String placa, psw;
    

    @EJB
    ElementoFacadeLocal elementoEJB;

    public void login() throws Exception {
        String contextPath = FacesContext.getCurrentInstance().getExternalContext().getApplicationContextPath();
        //LoginDAO dao;
        try {
            //dao = new LoginDAO();
            Dao d = new Dao();
            this.psw = d.encrypt(psw);
            Elemento aux = this.elementoEJB.login(placa,psw);
            if (aux != null) {
                this.elemento = aux;
                this.placa = this.psw = "";
                FacesContext.getCurrentInstance().getExternalContext().redirect(contextPath);
            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", "No tienes acceso al sistema."));
            }
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", "Usuario o contraseña inválidos"));
            //throw e;
        }
    }

    public void logout() throws IOException {
        this.setElemento(null);
        String contextPath = FacesContext.getCurrentInstance().getExternalContext().getApplicationContextPath();
        FacesContext.getCurrentInstance().getExternalContext().redirect(contextPath);
    }
    
    
    public void darAcceso() throws IOException{
        if(!this.loggedIn()){
            String cont = FacesContext.getCurrentInstance().getExternalContext().getApplicationContextPath();
            FacesContext.getCurrentInstance().getExternalContext().redirect(cont + "/faces/views/Login.xhtml");
        }
    }
    
    public void darAccesoMaestro() throws IOException{
        if(this.loggedIn()){
            if(!this.esAdminLogeado()){
                String contextPath = FacesContext.getCurrentInstance().getExternalContext().getApplicationContextPath();
                FacesContext.getCurrentInstance().getExternalContext().redirect(contextPath);
            }
        }else{
            String cont = FacesContext.getCurrentInstance().getExternalContext().getApplicationContextPath();
            FacesContext.getCurrentInstance().getExternalContext().redirect(cont + "/faces/views/Login.xhtml");
        }
    }
    
    public void darAccesoreporte() throws IOException{
        if (this.loggedIn()) {
            if (this.esAdminLogeado() || this.esMinisterioLogeado()) {
            }else{
                String contextPath = FacesContext.getCurrentInstance().getExternalContext().getApplicationContextPath();
                FacesContext.getCurrentInstance().getExternalContext().redirect(contextPath); 
            }
        } else {
            String cont = FacesContext.getCurrentInstance().getExternalContext().getApplicationContextPath();
            FacesContext.getCurrentInstance().getExternalContext().redirect(cont + "/faces/views/Login.xhtml");
        } 
    }
    


    public boolean loggedIn() {
        return (this.getElemento() != null) ? true : false;
    }
    
    public boolean esPoliciaLogeado() throws IOException{
        if(this.loggedIn() && this.elemento.getNivelAcceso()==0){
            return true;
        }else{
            //String contextPath = FacesContext.getCurrentInstance().getExternalContext().getApplicationContextPath();
            //FacesContext.getCurrentInstance().getExternalContext().redirect(contextPath);
            return false;
        }
    }
    
    public boolean esMinisterioLogeado() throws IOException {
        if (this.loggedIn() && this.elemento.getNivelAcceso() == 1) {
            return true;
        } else {
            //String contextPath = FacesContext.getCurrentInstance().getExternalContext().getApplicationContextPath();
            //FacesContext.getCurrentInstance().getExternalContext().redirect(contextPath);
            return false;
        }
    }
    
    public boolean esAdminLogeado() throws IOException {
        if (this.loggedIn() && this.elemento.getNivelAcceso() == 2) {
            return true;
        } else {
            //String contextPath = FacesContext.getCurrentInstance().getExternalContext().getApplicationContextPath();
            //FacesContext.getCurrentInstance().getExternalContext().redirect(contextPath);
            return false;
        }
    }


    public void redirctToLoginPage(String cont) throws IOException {
        FacesContext.getCurrentInstance().getExternalContext().redirect(cont + "/faces/views/Login.xhtml");
    }

    /**
     * @return the elemento
     */
    public Elemento getElemento() {
        return elemento;
    }

    /**
     * @param elemento the elemento to set
     */
    public void setElemento(Elemento elemento) {
        this.elemento = elemento;
    }

    /**
     * @return the placa
     */
    public String getPlaca() {
        return placa;
    }

    /**
     * @param placa the placa to set
     */
    public void setPlaca(String placa) {
        this.placa = placa;
    }

    /**
     * @return the psw
     */
    public String getPsw() {
        return psw;
    }

    /**
     * @param psw the psw to set
     */
    public void setPsw(String psw) {
        this.psw = psw;
    }

}
