/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itesm.Models;

import com.itesm.Dao.Dao;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.servlet.http.Part;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author jacobotapia
 */
@Entity
@Table(name = "Objeto")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Objeto.findAll", query = "SELECT o FROM Objeto o")
    , @NamedQuery(name = "Objeto.findByIdObjeto", query = "SELECT o FROM Objeto o WHERE o.idObjeto = :idObjeto")
    , @NamedQuery(name = "Objeto.findByHasImage", query = "SELECT o FROM Objeto o WHERE o.hasImage = :hasImage")})
public class Objeto implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idObjeto")
    private Integer idObjeto;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "descripcion")
    private String descripcion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "hasImage")
    private boolean hasImage;
    @Lob
    @Size(max = 65535)
    @Column(name = "imagen")
    private String imagen;
    @Lob
    @Size(max = 65535)
    @Column(name = "nombre")
    private String nombre;
    @JoinColumn(name = "reporteID", referencedColumnName = "idReporte")
    @ManyToOne(optional = false)
    private Reporte reporteID;
    @Transient
    private Part file;
    @Transient
    private boolean decrypted;

    public Objeto() {
        this.imagen = null;
        decrypted = false;
    }

    public Objeto(Integer idObjeto) {
        this.idObjeto = idObjeto;
    }

    public Objeto(Integer idObjeto, String descripcion, boolean hasImage) {
        this.idObjeto = idObjeto;
        this.descripcion = descripcion;
        this.hasImage = hasImage;
    }

    public Integer getIdObjeto() {
        return idObjeto;
    }

    public void setIdObjeto(Integer idObjeto) {
        this.idObjeto = idObjeto;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public boolean getHasImage() {
        return hasImage;
    }

    public void setHasImage(boolean hasImage) {
        this.hasImage = hasImage;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Reporte getReporteID() {
        return reporteID;
    }

    public void setReporteID(Reporte reporteID) {
        this.reporteID = reporteID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idObjeto != null ? idObjeto.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Objeto)) {
            return false;
        }
        Objeto other = (Objeto) object;
        if ((this.idObjeto == null && other.idObjeto != null) || (this.idObjeto != null && !this.idObjeto.equals(other.idObjeto))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.itesm.Models.Objeto[ idObjeto=" + idObjeto + " ]";
    }

    /**
     * @return the file
     */
    public Part getFile() {
        return file;
    }

    /**
     * @param file the file to set
     */
    public void setFile(Part file) {
        this.file = file;
    }
    
    public void decrypt() throws Exception {
        if(!decrypted){
            Dao d = new Dao();
            this.descripcion = d.decrypt(this.descripcion);
            this.nombre = d.decrypt(this.nombre);
        }
    }
    
    public void encrypt() throws Exception{
        Dao d = new Dao();
        this.descripcion = d.encrypt(this.descripcion);
        this.nombre = d.encrypt(this.nombre);
    }
    
}
