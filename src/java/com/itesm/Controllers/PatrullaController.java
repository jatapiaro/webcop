/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itesm.Controllers;

import com.itesm.Dao.Dao;
import com.itesm.Ejbs.PatrullaFacadeLocal;
import com.itesm.Models.Patrulla;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.primefaces.context.RequestContext;

/**
 *
 * @author jacobotapia
 */
@Named
@ViewScoped
public class PatrullaController implements Serializable{
    
    
    @EJB
    PatrullaFacadeLocal patrullaEJB;
    private Patrulla patrulla;
    private List<Patrulla> patrullas;
    
    @PostConstruct
    public void init(){
        this.patrulla = new Patrulla();
        this.patrullas = new ArrayList<Patrulla>();
    }
    
    public void listar() throws Exception{
        this.patrullas=this.patrullaEJB.findAllActivo();
        for(Patrulla p : patrullas){
            p.decrypt();
        }
    }
    
    public void delete(Integer id){
        this.patrullaEJB.delete(id);
    }
    
    public void findForEdit(Object id) throws Exception{
        this.patrulla = this.patrullaEJB.find(id);
        this.patrulla.decrypt();
    }
    
    public void edit() throws Exception{
        this.patrulla.encrypt();
        this.patrullaEJB.edit(patrulla);
        this.patrulla = null;
        this.patrulla = new Patrulla();
    }
    
    public void crear() throws Exception{
        Dao d = new Dao();
        String placas = d.encrypt(this.patrulla.getPlacas().toUpperCase());
        if(this.patrullaEJB.findByPlacas2(placas)==null){
            this.patrulla.setPlacas(this.patrulla.getPlacas().toUpperCase());
            this.patrulla.encrypt();
            this.patrulla.setActivo(true);
            this.patrullaEJB.create(patrulla);
            this.patrulla=null;
            this.patrulla=new Patrulla();
            RequestContext.getCurrentInstance().execute("PF('widgetDatos').hide();");
        }else{
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Estas placas ya están registradas", "Estas placas ya están registradas"));
        }
    }

    /**
     * @return the patrulla
     */
    public Patrulla getPatrulla() {
        return patrulla;
    }

    /**
     * @param patrulla the patrulla to set
     */
    public void setPatrulla(Patrulla patrulla) {
        this.patrulla = patrulla;
    }

    /**
     * @return the patrullas
     */
    public List<Patrulla> getPatrullas() {
        return patrullas;
    }

    /**
     * @param patrullas the patrullas to set
     */
    public void setPatrullas(List<Patrulla> patrullas) {
        this.patrullas = patrullas;
    }
    
    
}
