/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itesm.Models;

import com.itesm.Dao.Dao;
import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author jacobotapia
 */
@Entity
@Table(name = "Patrulla")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Patrulla.findAll", query = "SELECT p FROM Patrulla p")
    , @NamedQuery(name = "Patrulla.findByIdPatrulla", query = "SELECT p FROM Patrulla p WHERE p.idPatrulla = :idPatrulla")
    , @NamedQuery(name = "Patrulla.findByActivo", query = "SELECT p FROM Patrulla p WHERE p.activo = :activo")})
public class Patrulla implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idPatrulla")
    private Integer idPatrulla;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "placas")
    private String placas;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "marca")
    private String marca;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "modelo")
    private String modelo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "activo")
    private boolean activo;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "patrullaID")
    private List<ElementoReporte> elementoReporteList;
    @JoinColumn(name = "sectorID", referencedColumnName = "idSector")
    @ManyToOne(optional = false)
    private Sector sectorID;
    @Transient
    private boolean decrypted;

    public Patrulla() {
        this.sectorID = new Sector();
        this.decrypted = false;
    }

    public Patrulla(Integer idPatrulla) {
        this.idPatrulla = idPatrulla;
    }

    public Patrulla(Integer idPatrulla, String placas, String marca, String modelo, boolean activo) {
        this.idPatrulla = idPatrulla;
        this.placas = placas;
        this.marca = marca;
        this.modelo = modelo;
        this.activo = activo;
    }

    public Integer getIdPatrulla() {
        return idPatrulla;
    }

    public void setIdPatrulla(Integer idPatrulla) {
        this.idPatrulla = idPatrulla;
    }

    public String getPlacas() {
        return placas;
    }

    public void setPlacas(String placas) {
        this.placas = placas;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public boolean getActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    @XmlTransient
    public List<ElementoReporte> getElementoReporteList() {
        return elementoReporteList;
    }

    public void setElementoReporteList(List<ElementoReporte> elementoReporteList) {
        this.elementoReporteList = elementoReporteList;
    }

    public Sector getSectorID() {
        return sectorID;
    }

    public void setSectorID(Sector sectorID) {
        this.sectorID = sectorID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPatrulla != null ? idPatrulla.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Patrulla)) {
            return false;
        }
        Patrulla other = (Patrulla) object;
        if ((this.idPatrulla == null && other.idPatrulla != null) || (this.idPatrulla != null && !this.idPatrulla.equals(other.idPatrulla))) {
            return false;
        }
        return true;
    }
    
    public void encrypt() throws Exception {
        Dao d = new Dao();
        this.placas = d.encrypt(this.placas);
        this.marca = d.encrypt(this.marca);
    }

    public void decrypt() throws Exception {
        if(!this.decrypted){
            Dao d = new Dao();
            //System.out.println(this.placas + " - - " + this.marca);
            this.placas = d.decrypt(this.placas);
            this.marca = d.decrypt(this.marca);
            //System.out.println(this.placas + " - - " + this.marca);
            this.decrypted = true;
        }
    }

    @Override
    public String toString() {
        String s = this.placas+" - "+this.marca+" "+this.modelo;
        if(s.equals("null - null null")){
            return "";
        }else{
            return s;
        }
    }
    
}
