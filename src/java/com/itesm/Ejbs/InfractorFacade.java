/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itesm.Ejbs;

import com.itesm.Models.Infractor;
import com.itesm.Models.Patrulla;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author jacobotapia
 */
@Stateless
public class InfractorFacade extends AbstractFacade<Infractor> implements InfractorFacadeLocal {

    @PersistenceContext(unitName = "WebCopPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public InfractorFacade() {
        super(Infractor.class);
    }

    @Override
    public Infractor findByRfc(String rfc) {
        Query q = em.createQuery("SELECT i FROM Infractor i WHERE i.rfc = :r", Infractor.class);
        q.setParameter("r", rfc);
        return (Infractor) q.getSingleResult();
    }

    @Override
    public Infractor findByRfc2(String rfc) {
        try{
            Query q = em.createQuery("SELECT i FROM Infractor i WHERE i.rfc = :r", Infractor.class);
            q.setParameter("r", rfc);
            return (Infractor) q.getSingleResult();
        }catch(NoResultException e){
            return null;
        }
    }
    
    @Override
    public List<Infractor> findAll(){
        Query q = em.createQuery("SELECT i FROM Infractor i ORDER BY i.idInfractor DESC", Infractor.class);
        return q.getResultList();
    }
    
}
