/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itesm.Ejbs;

import com.itesm.Models.Camara;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author jacobotapia
 */
@Local
public interface CamaraFacadeLocal {

    void create(Camara camara);

    void edit(Camara camara);

    void remove(Camara camara);

    Camara find(Object id);

    List<Camara> findAll();

    List<Camara> findRange(int[] range);

    int count();
    
    void delete(Integer id);
    
    Camara findByIdentificador(int identificador);
}
