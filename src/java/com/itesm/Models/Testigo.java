/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itesm.Models;

import com.itesm.Dao.Dao;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author jacobotapia
 */
@Entity
@Table(name = "Testigo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Testigo.findAll", query = "SELECT t FROM Testigo t")
    , @NamedQuery(name = "Testigo.findByIdTestigo", query = "SELECT t FROM Testigo t WHERE t.idTestigo = :idTestigo")})
public class Testigo implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idTestigo")
    private Integer idTestigo;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "rfc")
    private String rfc;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "nombre")
    private String nombre;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "apellidoPaterno")
    private String apellidoPaterno;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "apellidoMaterno")
    private String apellidoMaterno;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "testimonio")
    private String testimonio;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "firma")
    private String firma;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "numeroExterior")
    private String numeroExterior;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "numeroInterior")
    private String numeroInterior;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "calle")
    private String calle;
    @JoinColumn(name = "reporteID", referencedColumnName = "idReporte")
    @ManyToOne(optional = false)
    private Reporte reporteID;
    @JoinColumn(name = "direccionID", referencedColumnName = "codigoPostal")
    @ManyToOne(optional = false)
    private Direccion direccionID;
    @Transient
    private boolean decrypted;

    public Testigo() {
        decrypted = false;
        this.direccionID = new Direccion();
    }

    public Testigo(Integer idTestigo) {
        this.idTestigo = idTestigo;
    }

    public Testigo(Integer idTestigo, String rfc, String nombre, String apellidoPaterno, String apellidoMaterno, String testimonio, String firma, String numeroExterior, String numeroInterior, String calle) {
        this.idTestigo = idTestigo;
        this.rfc = rfc;
        this.nombre = nombre;
        this.apellidoPaterno = apellidoPaterno;
        this.apellidoMaterno = apellidoMaterno;
        this.testimonio = testimonio;
        this.firma = firma;
        this.numeroExterior = numeroExterior;
        this.numeroInterior = numeroInterior;
        this.calle = calle;
    }

    public Integer getIdTestigo() {
        return idTestigo;
    }

    public void setIdTestigo(Integer idTestigo) {
        this.idTestigo = idTestigo;
    }

    public String getRfc() {
        return rfc;
    }

    public void setRfc(String rfc) {
        this.rfc = rfc;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidoPaterno() {
        return apellidoPaterno;
    }

    public void setApellidoPaterno(String apellidoPaterno) {
        this.apellidoPaterno = apellidoPaterno;
    }

    public String getApellidoMaterno() {
        return apellidoMaterno;
    }

    public void setApellidoMaterno(String apellidoMaterno) {
        this.apellidoMaterno = apellidoMaterno;
    }

    public String getTestimonio() {
        return testimonio;
    }

    public void setTestimonio(String testimonio) {
        this.testimonio = testimonio;
    }

    public String getFirma() {
        return firma;
    }

    public void setFirma(String firma) {
        this.firma = firma;
    }

    public String getNumeroExterior() {
        return numeroExterior;
    }

    public void setNumeroExterior(String numeroExterior) {
        this.numeroExterior = numeroExterior;
    }

    public String getNumeroInterior() {
        return numeroInterior;
    }

    public void setNumeroInterior(String numeroInterior) {
        this.numeroInterior = numeroInterior;
    }

    public String getCalle() {
        return calle;
    }

    public void setCalle(String calle) {
        this.calle = calle;
    }

    public Reporte getReporteID() {
        return reporteID;
    }

    public void setReporteID(Reporte reporteID) {
        this.reporteID = reporteID;
    }

    public Direccion getDireccionID() {
        return direccionID;
    }

    public void setDireccionID(Direccion direccionID) {
        this.direccionID = direccionID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idTestigo != null ? idTestigo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Testigo)) {
            return false;
        }
        Testigo other = (Testigo) object;
        if ((this.idTestigo == null && other.idTestigo != null) || (this.idTestigo != null && !this.idTestigo.equals(other.idTestigo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return this.nombre+" "+this.apellidoPaterno+" "+this.apellidoPaterno;
    }
    
    public String nombreCompleto(){
        return this.nombre+" "+this.apellidoPaterno+" "+this.apellidoPaterno; 
    }
    
    public void encrypt() throws Exception{
        Dao d = new Dao();
        this.nombre = d.encrypt(this.nombre);
        this.apellidoPaterno = d.encrypt(this.apellidoPaterno);
        this.apellidoMaterno = d.encrypt(this.apellidoMaterno);
        this.calle = d.encrypt(this.calle);
        this.rfc = d.encrypt(this.rfc);
        this.testimonio = d.encrypt(this.testimonio);
        if (this.numeroInterior != null) {
            this.numeroInterior = d.encrypt(this.numeroInterior);
        }
        this.numeroExterior = d.encrypt(this.numeroExterior);
    }
    
    public void decrypt() throws Exception {
        if(this.decrypted==false){
            Dao d = new Dao();
            this.nombre = d.decrypt(this.nombre);
            this.apellidoPaterno = d.decrypt(this.apellidoPaterno);
            this.apellidoMaterno = d.decrypt(this.apellidoMaterno);
            this.calle = d.decrypt(this.calle);
            this.rfc = d.decrypt(this.rfc);
            this.testimonio = d.decrypt(this.testimonio);
            if (this.numeroInterior != null) {
                this.numeroInterior = d.decrypt(this.numeroInterior);
            }
            this.direccionID.decrypt();
            this.numeroExterior = d.decrypt(this.numeroExterior);
            this.decrypted = true;
        }
    }
    
}
